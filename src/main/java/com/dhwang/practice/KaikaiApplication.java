package com.dhwang.practice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class KaikaiApplication {

    public static void main(String[] args) {
        SpringApplication.run(KaikaiApplication.class, args);
    }
}
