DROP TABLE IF EXISTS tour_user;
DROP SEQUENCE IF EXISTS user_id_seq;
CREATE SEQUENCE user_id_seq;


CREATE TABLE tour_user (
  user_id     BIGINT NOT NULL,
  user_name   VARCHAR(255) NOT NULL,
  login_name  VARCHAR(255) NOT NULL,
  password    VARCHAR(255) NOT NULL,
  role    VARCHAR(100) NOT NULL,
  created_time TIMESTAMP    NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (user_id)
);
