package services;

import org.hamcrest.CoreMatchers;
import org.junit.Test;

import static org.junit.Assert.assertThat;

public class HelloServiceTest {

    @Test
    public void first() {
        assertThat(11, CoreMatchers.is(11));
    }

    @Test
    public void second() {
        assertThat(13, CoreMatchers.is(13));
    }
}
