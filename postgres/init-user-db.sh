#!/bin/bash
set -e

psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" <<-EOSQL
CREATE USER practice WITH PASSWORD 'practice';
CREATE DATABASE practicedb OWNER practice;
GRANT ALL PRIVILEGES ON DATABASE practicedb to practice;
EOSQL


psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" -d "practicedb" <<-EOSQL
CREATE EXTENSION IF NOT EXISTS "uuid-ossp";
EOSQL